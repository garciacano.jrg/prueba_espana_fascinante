import React from 'react';
import menu from '../assets/menu.png';
import logo from '../assets/LOGO.png';
import search from '../assets/search.png';

const Header = () => {
    return (
    <header>
      <nav className="d-flex justify-content-around">
          <div className="d-flex align-items-center button-menu">
            <img src={menu} alt="boton-menu" />
          </div>
          <div className="d-flex align-items-center logo">
            <img src={logo} alt="logo-header" />
          </div>
          <div className="d-flex align-items-center search">
            <img src={search} alt="search-icon" />
          </div>
        </nav>
        <nav>
          <ul className="navigation-category__list text-uppercase text-white">
            <li>Category</li>
            <li>Category</li>
            <li>Category</li>
          </ul>
        </nav>
    </header>
    )
}

export default Header;

