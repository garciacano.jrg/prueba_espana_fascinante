import React from "react";
import data from "../data/data.json";

const Cover = () => Object.values(data.cover).map((item) => {
    return (
      <section key={item.post_id}>
        <article
          className="cover-mobile"
          style={{
            backgroundImage: `linear-gradient(180deg, rgba(255,255,255,1) 0%, rgba(255,255,255,1) 10%, rgba(0,0,0,0) 100%),
                url(${item.cover_image.url})`,
          }}
        >
          <div className="container">
            <div className="mb-5">
              <h2 className="category-cover-mobile">
                <span>{item.category.name}</span> {item.subcategory.name}
              </h2>
              <h1 className="title-cover-mobile">{item.title}</h1>
              <h2 className="summary-cover-mobile">{item.summary}</h2>
            </div>
            <div className="align-items-center d-flex justify-content-between text-white">
              <div>
                <p className="mb-0 text-white">{item.author.name}</p>
                <p className="text-nowrap mt-0 text-white">
                  <span className="date">Date </span> {item.readtime}
                </p>
              </div>
              <div className="d-flex justify-content-end align-items-center">
                <div className="facebook-logo-box">
                  <img
                    src="http://espanafascinante.com/test/icons/FB_white.png"
                    alt="logo-facebook-white"
                  />
                </div>
                <div className="share-logo-box">
                  <img
                    src="http://espanafascinante.com/test/icons/share_white.png"
                    alt="logo-share-white"
                  />
                </div>
              </div>
            </div>
          </div>
        </article>
        <article>
          <div className="desktop-hero-container mt-4 container w-100">
            <div className="desktop-hero-container__info">
              <div>
                <div className="d-flex align-items-center mb-4">
                  <h2 className="cover-category">{item.category.name}</h2>
                  <h2 className="cover-subcategory">
                    {item.subcategory.name}
                  </h2>
                </div>
                <h2 className="title-cover-desktop">{item.title}</h2>
                <h1 className="summary-cover-desktop">{item.summary}</h1>
              </div>
              <div className="d-flex justify-content-between align-items-center">
                <div>
                  <p className="text-nowrap">{item.author.name}</p>
                  <p>
                    <span>Date </span>
                    {item.readtime}
                  </p>
                </div>
                <div className="d-flex justify-content-end">
                  <div className="facebook-logo-box">
                    <img
                      src="http://espanafascinante.com/test/icons/FB.png"
                      alt="logo-facebook-white"
                    />
                  </div>
                  <div className="share-logo-box">
                    <img
                      src="http://espanafascinante.com/test/icons/share.png"
                      alt="logo-share-white"
                    />
                  </div>
                </div>
              </div>
            </div>
            <div>
              <img src={item.cover_image.url} alt="01" />
            </div>
          </div>
        </article>
      </section>
    );
  });

export default Cover;
